/**
 * @format
 */

import 'react-native';
import React from 'react';
import App from '../App';
import {getRandomInt,shuffleArray,getCardsData} from '../src/screens/MainScreen';
import ResetView from '../src/components/ResetView';
import renderer from 'react-test-renderer';

test('check Random Vals', () => {
    expect(getRandomInt(100)).toBeLessThan(100);
});

test(' Check Shuffle Array',() => {
    expect(shuffleArray([2,4,5,6])).not.toMatchObject([2,4,5,6]);
})

test(' Check Cards Data',() => {
    expect(getCardsData(100)).toHaveLength(4);
})


 it('ResetView renders correctly', () => {
   renderer.create( <ResetView color={'white'} restart={()=>{}}/>);
});