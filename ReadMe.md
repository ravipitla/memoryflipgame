* CARD_PAIRS_VALUE added in MainScreen.tsx - can edit for required pairs of cards.
* Tested it in only Android due to unavailablity of iOS ev.

This is [screenshot](https://drive.google.com/file/d/1IxPd5Rgu4fCUTTog7yVBJo8NxXQtw0Kg/view?usp=sharing) .
[screen recording](https://drive.google.com/file/d/1IqQCHVtCswDlG6OKhxRn9YgkPBXSxeQa/view?usp=sharing)
