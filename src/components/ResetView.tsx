import React, { Component, FC, useState } from 'react';
import { View, Text, StyleSheet, TouchableHighlight } from 'react-native';

interface Reset{
    color:string,
    restart():void,
}

const ResetView : FC<Reset> = props => {
    return (
<View style={styles.container} >
<TouchableHighlight
style = {styles.buttonStyle}
  activeOpacity={0.6}
  underlayColor="#DDDDDD"
  onPress={props.restart}>
  <Text style={styles.textStyle}>Restart</Text>
</TouchableHighlight>
</View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
       justifyContent:'center'      ,
        marginTop: 10,
        backgroundColor: '#fff'
    },
    buttonStyle:{
        backgroundColor:'#BF360C',
        justifyContent:'center',
        height:50,
        width:200,
        borderRadius:6,
    },
    textStyle:{
        fontSize:22,
        color:'white',
        fontWeight:'bold',
        justifyContent:'center',
        alignContent:'center',
       alignSelf:'center'
    }

});

export default ResetView;