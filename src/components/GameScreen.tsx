import React, { Component, FC, useState, useMemo } from 'react';
import { View, Text, StyleSheet, Alert } from 'react-native';
import Card from './Card';


interface Game {

    maxNumber: number,
    cardsAry: any[],
    restartCnt: number,
    restart(): void,
}
const getIndexAry = () => {
    let statsAry: boolean[] = [];
    while (statsAry.length < 13)
        statsAry.push(false);
    return statsAry;
}

const GameScreen: FC<Game> = props => {

    const [cardsStatusAry, setCardStatusAry] = useState(getIndexAry);
    const [clickCount, setClickCount] = useState(0);
    const [openCount, setOpenCount] = useState(0);
    const [openedCard, setOpenedCard] = useState({ id: -1, cardNo: -1 });
    const [stepsCount, setStepsCount] = useState(0);

    const restart = () => {
        setCardStatusAry(getIndexAry);
        setClickCount(0);
        setOpenCount(0);
        setOpenedCard({ id: -1, cardNo: -1 });
        setStepsCount(0);
    }

    const cardClick = (id: number, cardNo: number, isOpen: boolean) => {
        if (isOpen) {
            setStepsCount(stepsCount + 1)
            let localArray: boolean[] = cardsStatusAry;
            if (openCount + 1 === 2) {
                if (openedCard.cardNo === cardNo) {
                    localArray[id] = true;
                    localArray[openedCard.id] = true
                } else {
                    localArray[id] = false;
                    localArray[openedCard.id] = false;
                }
                //Do compare by ids
                //If not equal close 2 and init openCount
                setOpenCount(0);
            } else {
                setOpenedCard({ id: id, cardNo: cardNo })
                setOpenCount(openCount + 1);
                localArray[id] = true;
            }
            setCardStatusAry(localArray);
            let trueCount = 0;

            for (let val of localArray) {
                if (val)
                    trueCount++;
            }

            if (trueCount > (localArray.length - 2)) {
                Alert.alert(
                    'Flipcard Memory Game',
                    "Congratulations on winning the Game. Do you want to play again?",
                    [
                        {
                            text: "No",
                            onPress: () => console.log("Cancel Pressed"),
                            style: "cancel"
                        },
                        { text: "Yes", onPress: props.restart }
                    ]
                );
            }
        }


    }


    const checkRestart = useMemo(() => { restart() }, [props.restartCnt]);

    return (
        <View style={styles.container}>
            <Text style={styles.stepsTextStyle}>{`Steps : ${stepsCount}`}</Text>
            <View style={styles.body}>
                {props.cardsAry.map((rowItem, index) => {
                    return <View key={index} style={[styles.row]}>
                        {rowItem.map((item:{id:number,cardNo:number}) =>  <Card key={'card'+item.id} id={item.id} cardNo={item.cardNo} done={cardsStatusAry[index]} cardClick={cardClick} setClickCount={setClickCount} setCardStatusAry={setCardStatusAry} cardsStatusAry={cardsStatusAry} /> 
                        )}</View>
                })}
                            </View>
            {checkRestart}
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        marginTop: 10,
        backgroundColor: '#fff'
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        alignSelf: 'stretch',
        justifyContent: 'space-between',
        padding: 10,

    },
    body: {


        justifyContent: 'center',
        padding: 10,

    },
    stepsTextStyle: {
        flex: 1,
        flexDirection: 'row',
        fontSize: 22,
        marginRight: 20,
        color: 'green',
        fontWeight: 'bold',
        justifyContent: 'flex-end',
        alignContent: 'flex-end',
        alignSelf: 'flex-end'
    }

});


export default GameScreen;