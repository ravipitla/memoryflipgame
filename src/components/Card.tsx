import React, { FC, useRef, useEffect, useMemo} from 'react';
import { SafeAreaView, StyleSheet, Text, View, Image, Animated, Pressable } from 'react-native';



interface CardI {

  key?: string,
  id: number,
  cardNo: number
  cardsStatusAry: boolean[],
  done: boolean,
  cardClick(id:number,cardNo:number,isOpen:boolean):void,
  setClickCount(count: number): void,
  setCardStatusAry(cardStatusAry: boolean[]): void
}

const Card: FC<CardI> = props => {
  
  const flipAnimation = useRef(new Animated.Value(0)).current;
  let flipRotation = 0;
  flipAnimation.addListener(({ value }) => flipRotation = value);
  const flipToFrontStyle = {
    transform: [
      {
        rotateY: flipAnimation.interpolate({
          inputRange: [0, 180],
          outputRange: ["0deg", "180deg"]
        })
      }
    ]
  };
  const flipToBackStyle = {
    transform: [
      {
        rotateY: flipAnimation.interpolate({
          inputRange: [0, 180],
          outputRange: ["180deg", "360deg"]
        })
      }
    ]
  };

  const flipToFront = () => {
    Animated.timing(flipAnimation, {
      toValue: 180,
      duration: 300,
      useNativeDriver: true,
    }).start();
  };
  const flipToBack = () => {
    Animated.timing(flipAnimation, {
      toValue: 0,
      duration: 300,
      useNativeDriver: true,
    }).start();
  };
  const onCardClick = () => {
    if (props.cardsStatusAry[props.id]) {
    
      return;
    }

    if (!!flipRotation) {
      flipToBack();
      
    } else {
      flipToFront();
      
      setTimeout(()=>{  props.cardClick(props.id,props.cardNo,true)},350)
     
    }

  }
  const updateCard = useMemo(() => { setTimeout(()=>{  props.cardsStatusAry[props.id] ? flipToFront() : flipToBack()},350);
},[props]);
  

  return (
    <Pressable
      style={style.cardWrapper}
      onPress={onCardClick}
    >
      <Animated.View
        style={{ ...style.cardBack, ...flipToBackStyle, ...style.buttonStyle }}>
 <Text style={style.fontStyle}>{props.cardNo}</Text>
     

      </Animated.View>


      <Animated.View
        style={{...style.cardFront,  ...flipToFrontStyle, ...style.buttonStyle }}>

<Text style={style.fontStyle}>?</Text>


      </Animated.View>
      {updateCard}
    </Pressable>
  );
}
const style = StyleSheet.create({
  cardWrapper: { width: 100, height: 100 },
  cardFront: {
    backgroundColor: '#01579B',
    borderColor: '#D81B60',
    backfaceVisibility: "hidden"
    
  },
  cardBack: {
    backgroundColor: '#D81B60',
    borderColor: '#01579B',
    position: "absolute",
  },
  buttonStyle: { width: 100, height: 100, justifyContent: 'center', borderWidth: 0, borderRadius: 8, margin: 10 },
  fontStyle: { fontSize: 40, fontWeight: "bold", justifyContent: 'center', color: 'white', alignSelf: 'center' }
});

export default Card;