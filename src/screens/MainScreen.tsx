import React, { Component, FC, useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import GameScreen from '../components/GameScreen';
import ResetView from '../components/ResetView';

export const getRandomInt = (max: number): number => {
    return Math.floor(Math.random() * max);
}

export const shuffleArray = (array: number[]) => {
    for (var i = array.length - 1; i > 0; i--) {

        // Generate random number
        var j = Math.floor(Math.random() * (i + 1));

        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    return array;
}

export const getCardsData= (maxNumber:number) => {
let randomNos: number[] = [];
const CARD_PAIRS_VALUE = 6
while (randomNos.length < CARD_PAIRS_VALUE) {
    let rVal: number = getRandomInt(maxNumber);       
    if (randomNos.length == 0)
        randomNos.push(rVal);
    else {
        if (randomNos.includes(rVal))
            while (randomNos.includes(rVal))
                rVal = getRandomInt(maxNumber);
        randomNos.push(rVal);
    }
}
let dupAry: number[] = shuffleArray(randomNos.concat(randomNos));
let cardsAry: {id:number,cardNo:number}[] = [];
    for(let i=0; i<dupAry.length;i++){
        cardsAry.push({id:i,cardNo:dupAry[i]})
    }
    const COLUMNS = 3;
    let cardsDDAry:any[] = [];
    for(let i=0;i<cardsAry.length;i+COLUMNS){       
        cardsDDAry.push(cardsAry.splice(i,i+COLUMNS))
    }
    
return cardsDDAry;
}







interface MainScreenProp {

    maxNumber: number,
}

const MainScreen:FC<MainScreenProp> =props =>{
    let maxNo = props.maxNumber ? props.maxNumber : 100; 
    const [cardsAry,setCardsAry] = useState(getCardsData(maxNo));
    const [restartCnt,setRestartCnt] = useState(0);
    
    const restart =() =>{
       
        setCardsAry(getCardsData(maxNo));
        setRestartCnt(restartCnt+1);
    }
    return (
        <View style={styles.container}>
          
            <GameScreen maxNumber={maxNo} cardsAry={cardsAry} restartCnt={restartCnt}  restart={restart} />
            <ResetView color={'white'} restart={restart}/>
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
           
        marginTop: 10,
        backgroundColor: '#fff'
    },
    buttonStyle:{
        backgroundColor:'#BF360C',
        justifyContent:'center',
        height:30,
        width:170
    },
    textStyle:{
        fontSize:22,
        color:'white',
        justifyContent:'center'
    },
    

});
export default MainScreen;
